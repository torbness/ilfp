#!/usr/bin/env python
'''
Returns cell object
'''
import LFPy
import numpy as np
from os.path import join
import neuron
import pylab as plt
nrn = neuron.h

def _make_WN_input(cell, max_freq):
    """ White Noise input ala Linden 2010 is made """
    tot_ntsteps = round((cell.tstopms - cell.tstartms)/\
                  cell.timeres_NEURON + 1)
    I = np.zeros(tot_ntsteps)
    tvec = np.arange(tot_ntsteps) * cell.timeres_NEURON
    plt.seed(1234)
    for freq in xrange(1, max_freq + 1):
        I += np.sin(2 * np.pi * freq * tvec/1000. + 2*np.pi*np.random.random())
    return I

def insert_white_noise(cell, input_idx, weight):

    max_freq = 500
    input_array = weight * (_make_WN_input(cell, max_freq) - 10)

    noise_vec = neuron.h.Vector(input_array)
    i = 0
    syn = None
    for sec in cell.allseclist:
        for seg in sec:
            if i == input_idx:
                print "Input inserted in ", sec.name()
                syn = neuron.h.ISyn(seg.x, sec=sec)
            i += 1
    if syn is None:
        raise RuntimeError("Wrong stimuli index")
    syn.dur = 1E9
    syn.delay = 0
    noise_vec.play(syn._ref_amp, cell.timeres_NEURON)
    return cell, [syn, noise_vec]


def insert_synapse(cell, input_idx):
    # Define synapse parameters
    synapse_parameters = {
        'idx': input_idx,
        'e': 0.,                   # reversal potential
        'syntype': 'ExpSyn',       # synapse type
        'tau': 2.,                # syn. time constant
        'weight': .1,            # syn. weight
        'record_current': True,
    }

    # Create synapse and set time of synaptic input
    synapse = LFPy.Synapse(cell, **synapse_parameters)
    synapse.set_spike_times(np.array([5.]))
    return cell, synapse



def infinite_axon_quasi_active(**kwargs):


    for sec in neuron.h.allsec():
        sec.nseg = 1
        sec.insert("QA")
        sec.V_r_QA = -80
        sec.tau_w_QA = 50.
        sec.Ra = 100
        sec.cm = 1.0
        sec.g_pas_QA = 0.00005

    # total_area = _get_total_area()
    total_w_conductance =  0.00005 * 2 * 20
    # max_dist = _get_longest_distance()

    # decrease_factor = 60
    # conductance_factor = _get_linear_decrease_factor(decrease_factor, max_dist, total_w_conductance)
    nrn.distance(0, 0.5)
    # for sec in neuron.h.allsec():
    #     for seg in sec:
    #         seg.g_w_bar_QA = conductance_factor*(decrease_factor - (decrease_factor - 1.) * nrn.distance(seg.x) / max_dist)


    for sec in neuron.h.allsec():


        #sec.g_w_bar_QA = 0.00005 * 20#2 / 30000.
        for seg in sec:
            for seg in sec:
                seg.g_w_bar_QA = total_w_conductance

            if neuron.h.distance(seg.x) <= 50:
                seg.mu_QA = sec.g_w_bar_QA / sec.g_pas_QA * kwargs['mu_factor_1']
            else:
                seg.mu_QA = sec.g_w_bar_QA / sec.g_pas_QA * kwargs['mu_factor_2']

def return_cell(stimuli='synapse', cell_name='mainen', input_idx=0, weight=0.001, mu_factor_1=None, mu_factor_2=None):

    if cell_name is 'mainen':
        cell_parameters = {          # various cell parameters,
            'morphology': 'L5_Mainen96_LFPy.hoc', # Mainen&Sejnowski, 1996
            'rm': 30000.,      # membrane resistance
            'cm': 1.0,         # membrane capacitance
            'Ra': 150.,        # axial resistance
            'v_init': -65.,    # initial crossmembrane potential
            'e_pas': -65.,     # reversal potential passive mechs
            'passive': True,   # switch on passive mechs
            'nsegs_method': 'lambda_f',
            'lambda_f': 100.,
            'timeres_NEURON': 2.**-4,   # [ms] dt's should be in powers of 2 for both,
            'timeres_python': 2.**-4,   # need binary representation
            'tstartms': 0.,
            'tstopms': 2000.,
        }
    elif cell_name is 'infinite_axon':
        cell_parameters = {          # various cell parameters,
            'morphology': 'infinite_axon.hoc',
            'v_init': -80.,    # initial crossmembrane potential
            'passive': False,   # switch on passive mechs
            'nsegs_method': 'lambda_f',
            'lambda_f': 500.,
            'timeres_NEURON': 2.**-3,   # [ms] dt's should be in powers of 2 for both,
            'timeres_python': 2.**-3,   # need binary representation
            'custom_fun': [infinite_axon_quasi_active],
            'custom_fun_args': [{'mu_factor_1': mu_factor_1, 'mu_factor_2': mu_factor_2}],
            'tstartms': 0.,
            'tstopms': 5000.,
        }
    else:
        raise RuntimeError("Unknown cell")

    # Create cell
    cell = LFPy.Cell(**cell_parameters)

    if stimuli == 'synapse':
        cell, stim = insert_synapse(cell, input_idx, weight)
    elif stimuli == 'white_noise':
        cell, stim = insert_white_noise(cell, input_idx, weight)
    else:
        raise ValueError("Unknown input stimuli type")
    cell.simulate(rec_imem=True, rec_vmem=True)
    keep_tsteps = len(cell.tvec)/2

    folder = cell_name
    sim_name = '%s_%s_%d_%1.1f_%1.1f' % (cell_name, stimuli, input_idx, mu_factor_1, mu_factor_2)
    np.save(join(folder, 'tvec_%s_%s.npy' % (cell_name, stimuli)), cell.tvec[-keep_tsteps:] - cell.tvec[-keep_tsteps])
    np.save(join(folder, 'imem_%s.npy' % sim_name), cell.imem[:, -keep_tsteps:])
    np.save(join(folder, 'vmem_%s.npy' % sim_name), cell.vmem[:, -keep_tsteps:])
    np.save(join(folder, 'xstart_%s.npy' % cell_name), cell.xstart)
    np.save(join(folder, 'ystart_%s.npy' % cell_name), cell.ystart)
    np.save(join(folder, 'zstart_%s.npy' % cell_name), cell.zstart)
    np.save(join(folder, 'xend_%s.npy' % cell_name), cell.xend)
    np.save(join(folder, 'yend_%s.npy' % cell_name), cell.yend)
    np.save(join(folder, 'zend_%s.npy' % cell_name), cell.zend)
    np.save(join(folder, 'xmid_%s.npy' % cell_name), cell.xmid)
    np.save(join(folder, 'ymid_%s.npy' % cell_name), cell.ymid)
    np.save(join(folder, 'zmid_%s.npy' % cell_name), cell.zmid)
    np.save(join(folder, 'diam_%s.npy' % cell_name), cell.diam)

    cell.imem = cell.imem[:, -keep_tsteps:]
    cell.vmem = cell.vmem[:, -keep_tsteps:]
    cell.tvec = cell.tvec[-keep_tsteps:] - cell.tvec[-keep_tsteps]

    return cell

if __name__ == '__main__':
    return_cell()