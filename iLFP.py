from __future__ import division
__author__ = 'tone'
import sys
import os
import numpy as np
from os.path import join
# from plotting_convention import *
import scipy.fftpack as ff
# import aLFP
import neuron
import LFPy
import pylab as plt
from return_cell import return_cell
from matplotlib.widgets import Button
import Tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

class SimulationExperiment():
    """ Class for holding different simulation runs with the same cell, to be used for plotting by InteractiveLFP class.
        Needs name to cell info, and name of individual simulations
    """
    def __init__(self, root_dir, cell_name, stimuli, input_idx, simulation_list, repeats, sim_type):
        self.cell_name = cell_name
        if sim_type is 'hu':
            self.cell_folder = sim_type
        else:
            self.cell_folder = cell_name

        self.root_dir = root_dir
        self.repeats = repeats
        self.stimuli = stimuli
        self.input_idx = input_idx
        self.sim_type = sim_type
        self.experiment_dict = {}
        self.simulation_list = simulation_list
        self._load_common_cell_parameters()
        self._load_simulation_list_data()

    def _load_common_cell_parameters(self):
        if self.sim_type in ['hay', 'hu', 'infinite_neurite'] and type(self.input_idx) is int:
            tvec = np.load(join(self.root_dir, self.cell_folder, 'tvec_%s.npy' % self.cell_name))
        else:
            tvec = np.load(join(self.root_dir, self.cell_folder, 'tvec_%s_%s.npy' % (self.cell_name, self.stimuli)))

        print "Time vector length: %d" % len(tvec)
        self.cut_off_idx = (len(tvec)) / self.repeats if not self.repeats is None else len(tvec)
        self.tvec = tvec[-self.cut_off_idx:] - tvec[-self.cut_off_idx]
        if self.sim_type is 'generic':
            distinguisher = '%s_%s' % (self.cell_name, self.sim_type)
        else:
            distinguisher = self.cell_name

        self.xstart = np.load(join(self.root_dir, self.cell_folder, 'xstart_%s.npy' % distinguisher))
        self.ystart = np.load(join(self.root_dir, self.cell_folder, 'ystart_%s.npy' % distinguisher))
        self.zstart = np.load(join(self.root_dir, self.cell_folder, 'zstart_%s.npy' % distinguisher))
        self.xend = np.load(join(self.root_dir, self.cell_folder, 'xend_%s.npy' % distinguisher))
        self.yend = np.load(join(self.root_dir, self.cell_folder, 'yend_%s.npy' % distinguisher))
        self.zend = np.load(join(self.root_dir, self.cell_folder, 'zend_%s.npy' % distinguisher))
        self.xmid = np.load(join(self.root_dir, self.cell_folder, 'xmid_%s.npy' % distinguisher))
        self.ymid = np.load(join(self.root_dir, self.cell_folder, 'ymid_%s.npy' % distinguisher))
        self.zmid = np.load(join(self.root_dir, self.cell_folder, 'zmid_%s.npy' % distinguisher))
        self.diam = np.load(join(self.root_dir, self.cell_folder, 'diam_%s.npy' % distinguisher))
        
        self.totnsegs = len(self.xmid)


    def _load_simulation_list_data(self):

        input = str(self.input_idx)

        for simulation in self.simulation_list:

            # sim_name = '%s_%s_%s' % (self.cell_name, input, simulation)
            if self.sim_type in ['generic', 'mainen', 'infinite_axon'] or type(self.input_idx) is str:
                sim_name = '%s_%s_%s_%s' % (self.cell_name, self.stimuli, input, simulation)
            # elif self.sim_type is 'infinite_neurite':
            #     sim_name = '%s_%s_%s_-80_0.0001' % (self.cell_name, input, simulation)
            # else:
            #     sim_name = '%s_%s_%s' % (self.cell_name, input, simulation)
            self.experiment_dict[simulation] = DummyCell(join(self.root_dir, self.cell_folder), sim_name, self.cut_off_idx)

class DummyCell():
    def __init__(self, folder, sim_name, cut_off_idx):
        self.name = sim_name
        print "Making dummy cell ", self.name, folder
        self.imem = np.load(join(folder, 'imem_%s.npy' % sim_name))[:, -cut_off_idx:]
        self.vmem = np.load(join(folder, 'vmem_%s.npy' % sim_name))[:, -cut_off_idx:]


class InteractiveLFP():
    # TODO: ADD TIME ZOOMING POSSIBILITY
    # TODO: Document better

    def __init__(self, cell, input_idx):

        self.LFP_dict = {}
        self.imem_dict = {}
        self.vmem_dict = {}
        self.color_dict = {}
        self.v_lines_dict = {}
        self.i_lines_dict = {}
        self.e_lines_dict = {}
        self.color_codes = {'passive': 'k',
                           'Ih': 'b',
                           'NaP': 'orange',
                           'active': 'r',
                           'Im': 'g'}
        self.exp_name_dict = {'-0.5': 'Regenerative ($\mu = -0.5$)',
                              '0.0': 'Passive ($\mu = 0$)',
                              '2.0': 'Restorative ($\mu = 2$)'}
        self.line_styles = {}
        if hasattr(cell, 'experiment_dict'):
            self.compare_mode = True
            self.experiment_dict = cell.experiment_dict
        else:
            self.compare_mode = False
            cell.sim_type = 'testing'
            self.experiment_dict = {'cell': cell}
        self.cell = cell
        self.tvec = self.cell.tvec
        self.num_comps = self.cell.totnsegs
        self.xmid = self.cell.xmid
        self.ymid = self.cell.ymid
        self.zmid = self.cell.zmid
        self.xstart = self.cell.xstart
        self.ystart = self.cell.ystart
        self.zstart = self.cell.zstart
        self.xend = self.cell.xend
        self.yend = self.cell.yend
        self.zend = self.cell.zend

        print len(self.xmid), self.num_comps

        for numb, exp in enumerate(self.experiment_dict):
            self.imem_dict[exp] = self.experiment_dict[exp].imem
            self.vmem_dict[exp] = self.experiment_dict[exp].vmem
            self._set_exp_line_properties(exp)

        timestep = (self.tvec[1] - self.tvec[0])/1000.
        sample_freq = ff.fftfreq(len(self.tvec), d=timestep)
        self.pidxs = np.where(sample_freq >= 0)[0]
        self.freqs = sample_freq[self.pidxs]

        self.prev_intra_comp = 0
        self.intra_comp = 0
        self.plot_t_start = 0
        self.plot_t_end = 1000
        self.input_idx = input_idx
        self.LFP_pos = [self.xmid[self.input_idx] + 50, self.ymid[self.input_idx],
                        self.zmid[self.input_idx]]
        self._calc_LFP()
        self.plot_psd = False
        self.plot_psd_prev = False
        self.changed_sig_domain = False
        self.fig_info_text = ("Input idx: %d\nIntracellular idx: %s, position: [%1.2f, %1.2f, %1.2f]\n" +
                              "LFP position: [%1.2f, %1.2f, %1.2f]")
        self._set_up_master_frame()
        self._initialize_figure()
        self.master.mainloop()

    def _set_up_master_frame(self):
        self.master = tk.Tk()
        self.master.title("Interactive LFP")

        toolbar = tk.Frame(self.master)
        toolbar.pack(side=tk.TOP, fill="x")

        tk.Label(toolbar, text="Idx: ").pack(side="left")
        self.comp_entry = tk.Entry(toolbar, width=4)
        self.comp_entry.pack(side="left")
        self.comp_entry.insert(0, '%d' % self.intra_comp)
        self.comp_entry.bind('<Return>', lambda d: self._update_plot())

        tk.Label(toolbar, text=" LFP pos [xyz]: ").pack(side="left")
        self.LFP_entry_x = tk.Entry(toolbar, width=6)
        self.LFP_entry_y = tk.Entry(toolbar, width=6)
        self.LFP_entry_z = tk.Entry(toolbar, width=6)
        self.LFP_entry_x.pack(side="left")
        self.LFP_entry_y.pack(side="left")
        self.LFP_entry_z.pack(side="left")

        self.LFP_entry_x.insert(0, '%1.2f' % self.LFP_pos[0])
        self.LFP_entry_y.insert(0, '%1.2f' % self.LFP_pos[1])
        self.LFP_entry_z.insert(0, '%1.2f' % self.LFP_pos[2])

        self.LFP_entry_x.bind('<Return>', lambda d: self._update_plot())
        self.LFP_entry_y.bind('<Return>', lambda d: self._update_plot())
        self.LFP_entry_z.bind('<Return>', lambda d: self._update_plot())

        tk.Button(toolbar, text="Update", width=7, command=self._update_plot).pack(side='left')
        #tk.Button(toolbar, text="Save figure", width=10, command=self._save_figure_popup).pack(side='left')
        tk.Button(toolbar, text="Reset zoom", width=6, command=self._zoom_out).pack(side='left')
        tk.Button(toolbar, text="Quit", command=self.master.quit).pack(side="right")

        self.sig_domain = tk.StringVar()
        self.sig_domain.set("Time")
        tk.Radiobutton(toolbar, text="PSD", variable=self.sig_domain,
                       indicatoron=0, value="PSD", command=self._update_plot).pack(side="left")
        time_button = tk.Radiobutton(toolbar, text="Time", variable=self.sig_domain,
                                     indicatoron=0, value="Time", command=self._update_plot)
        time_button.pack(side="left")
        time_button.select()

        self.popup = tk.Frame(self.master)
        self.popup.pack(side=tk.BOTTOM, fill="x")
        tk.Label(self.popup, text="Save figure snapshot: ").pack(side='left')
        tk.Label(self.popup, text="Folder: ").pack(side='left')

        self.root_folder_entry = tk.Entry(self.popup)
        self.root_folder_entry.pack(side='left')
        self.root_folder_entry.insert(0, '.')

        tk.Label(self.popup, text="Name: ").pack(side='left')
        self.figname_entry = tk.Entry(self.popup)
        self.figname_entry.pack(side='left')
        self.figname_entry.insert(0, '1.png')

        tk.Button(self.popup, text="Save", width=7, command=self._save_figure).pack(side='left')
        self.figname_entry.bind('<Return>', self._save_figure)


    def _initialize_figure(self):
        plt.close('all')
        #plt.ion()
        self.fig = plt.figure(figsize=[12, 12])
        self.dataplot = FigureCanvasTkAgg(self.fig, master=self.master)
        self.dataplot.get_tk_widget().pack(side=tk.TOP, expand=0.6)
        self.fig.subplots_adjust(bottom=0.2)

        self.fig.canvas.mpl_connect('button_press_event', self._mouse_click_at_position)
        self.fig.canvas.mpl_connect('scroll_event', self._mouse_scroll_at_position)

        self.ax1 = self.fig.add_subplot(121, aspect='equal', xlabel='x [$\mu m$]', ylabel='z [$\mu m$]')
        self.ax_v = self.fig.add_subplot(322, title='Membrane potential [mV]', xlabel='Time [$ms$]')
        self.ax_i = self.fig.add_subplot(324, title='Transmembrane current [nA]', xlabel='Time [$ms$]')
        self.ax_e = self.fig.add_subplot(326, title='LFP [$\mu V$]', xlabel='Time [$ms$]')
        idx = int(self.intra_comp)
        x = self.xmid[idx]
        y = self.ymid[idx]
        z = self.zmid[idx]
        self.fig_info = self.fig.text(0.02, 0.05, self.fig_info_text % (self.input_idx, idx, x, y, z, self.LFP_pos[0],
                                      self.LFP_pos[1], self.LFP_pos[2]), ha='left')

        self.fig.text(0.99, 0.05, "Scroll to zoom\nLeft click chooses closest compartment\n" +
                                  "Right click sets LFP position", ha='right', color='gray')

        self.morph_lines = []
        self.ax1.plot(self.xmid[self.input_idx], self.zmid[self.input_idx], 'y*', zorder=1, ms=20)

        for idx in xrange(self.num_comps):
            if idx == self.intra_comp:
                line_dict = {'c': 'r',
                             'lw': 3,
                             'zorder': 2,
                             'picker': 5}
            else:
                line_dict = {'c': 'gray',
                             'lw': 1,
                             'zorder': 0,
                             'picker': 5}

            l, = self.ax1.plot([self.xstart[idx], self.xend[idx]],
                               [self.zstart[idx], self.zend[idx]],
                               **line_dict)
            self.morph_lines.append(l)

        self.LFP_pos_marker, = self.ax1.plot(self.LFP_pos[0], self.LFP_pos[1], 'gD')
        self._zoom_out()

        lines = []
        line_names = []
        for exp in self.experiment_dict:
            _line_dict = {'c': self.color_dict[exp],
                          'ls': self.line_styles[exp]}
            self.v_lines_dict[exp], = self.ax_v.plot(self.tvec, self.vmem_dict[exp][self.intra_comp], **_line_dict)
            self.i_lines_dict[exp], = self.ax_i.plot(self.tvec, self.vmem_dict[exp][self.intra_comp], **_line_dict)
            self.e_lines_dict[exp], = self.ax_e.plot(self.tvec, self.LFP_dict[exp], **_line_dict)
            lines.append(self.v_lines_dict[exp])
            line_names.append(exp)
        self.fig.legend(lines, line_names, ncol=2, frameon=False)

        self._update_intracellular_plots()
        self._update_LFP_plot()
        self.dataplot.show()

    def _set_exp_line_properties(self, exp):
        if 'passive' in exp:
            self.color_dict[exp] = 'k'
        elif 'Im' in exp:
            self.color_dict[exp] = 'g'
        elif 'active' in exp:
            self.color_dict[exp] = 'r'
        elif 'Ih' in exp:
            self.color_dict[exp] = 'b'
        elif 'NaP' in exp:
            self.color_dict[exp] = 'orange'
        elif 'generic' in self.cell.sim_type:
            if '-0.5_' in exp:
                self.color_dict[exp] = 'r'
            elif '2.0_' in exp:
                self.color_dict[exp] = 'b'
            elif '0.0_' in exp:
                self.color_dict[exp] = 'k'
        elif 'axon' in self.cell.sim_type or 'neurite' in self.cell.sim_type:
            if '-0.5' in exp:
                self.color_dict[exp] = 'r'
            elif '2.0' in exp:
                self.color_dict[exp] = 'b'
            elif '0.0' in exp:
                self.color_dict[exp] = 'k'
        else:
            self.color_dict[exp] = 'k'

        if 'frozen' in exp:
            self.line_styles[exp] = '--'
        elif 'linearized' in exp:
            self.line_styles[exp] = ':'
        else:
            self.line_styles[exp] = '-'

    def _zoom_out(self):
        self.ax1.set_xlim(-500, 500)
        self.ax1.set_ylim(np.min(self.zend) - 100, np.max(self.zend) + 100)
        self.dataplot.draw()

    def _scroll_zoom_in(self, x, z):
        prev_xlim = self.ax1.get_xlim()
        prev_zlim = self.ax1.get_ylim()
        x_range = prev_xlim[1] - prev_xlim[0]
        z_range = prev_zlim[1] - prev_zlim[0]
        self.ax1.set_xlim(x - x_range * 0.25, x + x_range * 0.25)
        self.ax1.set_ylim(z - z_range * 0.25, z + z_range * 0.25)
        self.dataplot.draw()

    def _scroll_zoom_out(self, x, z):
        prev_xlim = self.ax1.get_xlim()
        prev_zlim = self.ax1.get_ylim()
        x_range = prev_xlim[1] - prev_xlim[0]
        z_range = prev_zlim[1] - prev_zlim[0]
        self.ax1.set_xlim(x - x_range * 0.75, x + x_range * 0.75)
        self.ax1.set_ylim(z - z_range * 0.75, z + z_range * 0.75)
        self.dataplot.draw()

    def _mouse_click_at_position(self, mouse_click):
        if mouse_click.button == 1:
            comp = self._find_closest_cell_compartment(mouse_click.xdata, mouse_click.ydata)
            self.comp_entry.delete(0, tk.END)
            self.comp_entry.insert(0, comp)
        elif mouse_click.button == 3:
            x, z = mouse_click.xdata, mouse_click.ydata
            self.LFP_entry_x.delete(0, tk.END)
            self.LFP_entry_z.delete(0, tk.END)
            self.LFP_entry_x.insert(0, '%1.2f' % float(x))
            self.LFP_entry_z.insert(0, '%1.2f' % float(z))
            self._set_LFP()
        self._update_plot()

    def _mouse_scroll_at_position(self, mouse_click):

        x, z = mouse_click.xdata, mouse_click.ydata
        if mouse_click.button == 'down':
            self._scroll_zoom_out(x, z)
        elif mouse_click.button == 'up':
            self._scroll_zoom_in(x, z)


    def _update_plot(self):
        self._set_signal_domain()
        self._set_comp()
        self._set_LFP()
        self._update_text_info()
        self.dataplot.draw()

    def _update_text_info(self):
        idx = int(self.intra_comp)
        x = self.xmid[idx]
        y = self.ymid[idx]
        z = self.zmid[idx]
        self.fig_info.set_text(self.fig_info_text % (self.input_idx, idx, x, y, z, self.LFP_pos[0],
                                                     self.LFP_pos[1], self.LFP_pos[2]))

    def _set_comp(self):
        comp = int(self.comp_entry.get())
        if comp == self.intra_comp and not self.changed_sig_domain:
            return
        self.prev_intra_comp = self.intra_comp
        if comp >= self.num_comps:
            self.intra_comp = self.num_comps - 1
        elif comp < 0:
            self.intra_comp = 0
        else:
            self.intra_comp = comp
        self._update_intracellular_plots()
        self.comp_entry.delete(0, tk.END)
        self.comp_entry.insert(0, self.intra_comp)

    def _set_LFP(self):
        pos_x = float(self.LFP_entry_x.get())
        pos_y = float(self.LFP_entry_y.get())
        pos_z = float(self.LFP_entry_z.get())
        pos = [pos_x, pos_y, pos_z]

        if pos == self.LFP_pos and not self.changed_sig_domain:
            # print "Not updating LFP plot"
            return
        # else:
        #     print "Updating LFP plot"

        if not pos == self.LFP_pos:
            self.LFP_pos = pos
            self._calc_LFP()
        self._update_LFP_plot()

    def _set_signal_domain(self):

        if self.sig_domain.get() == "Time":
            _plot_psd = False
        elif self.sig_domain.get() == "PSD":
            _plot_psd = True
        else:
            raise ValueError("Unknown signal domain. Should be 'Time' or 'PSD', got: %s" % self.sig_domain.get())
        self.plot_psd_prev = self.plot_psd
        self.plot_psd = _plot_psd
        if self.plot_psd_prev == self.plot_psd:
            self.changed_sig_domain = False
        else:
            self.changed_sig_domain = True

    def _rotate_view(self):
        raise NotImplementedError

    def _find_closest_cell_compartment(self, x, z):
        return np.argmin((x - self.xmid)**2 + (z - self.zmid)**2)

    def _calc_LFP(self):
        for exp in self.experiment_dict:
            print self.imem_dict[exp].shape, self.LFP_pos
            self.LFP_dict[exp] = self._calc_lfp_pointsource(self.imem_dict[exp], x=self.LFP_pos[0],
                                                            y=self.LFP_pos[1], z=self.LFP_pos[2], sigma=0.3)

    def _update_LFP_plot(self):
        for exp in self.experiment_dict:
            if self.plot_psd:
                psd = self.return_psd(self.LFP_dict[exp])
                self.e_lines_dict[exp].set_data([self.freqs, psd])
            else:
                self.e_lines_dict[exp].set_data([self.tvec, self.LFP_dict[exp]])

        if self.plot_psd:
            self.ax_e.set_yscale('log')
            self.ax_e.set_xscale('log')
            self.ax_e.set_xlabel('Frequency [$Hz$]')
            self.ax_e.set_xlim(self.freqs[1], 500)
            max_exponent = np.ceil(np.log10(np.max([np.max(l.get_ydata()[1:]) for l in self.ax_e.get_lines()])))
            self.ax_e.set_ylim([10**(max_exponent - 4), 10**max_exponent])
        else:
            self.ax_e.set_yscale('linear')
            self.ax_e.set_xscale('linear')
            self.ax_e.set_xlabel('Time [$ms$]')
            self.ax_e.set_xlim(self.plot_t_start, self.plot_t_end)

            lim = np.max([np.max(np.abs(l.get_ydata())) for l in self.ax_e.get_lines()])
            self.ax_e.set_ylim(-lim*1.2, lim*1.2)

        self.LFP_pos_marker.set_xdata(self.LFP_pos[0])
        self.LFP_pos_marker.set_ydata(self.LFP_pos[2])

    def _update_intracellular_plots(self):
        for exp in self.experiment_dict:
            if self.plot_psd:
                psd_i = self.return_psd(self.imem_dict[exp][self.intra_comp])
                psd_v = self.return_psd(self.vmem_dict[exp][self.intra_comp])
                self.i_lines_dict[exp].set_data([self.freqs, psd_i])
                self.v_lines_dict[exp].set_data([self.freqs, psd_v])
            else:
                self.i_lines_dict[exp].set_data([self.tvec, self.imem_dict[exp][self.intra_comp]])
                self.v_lines_dict[exp].set_data([self.tvec, self.vmem_dict[exp][self.intra_comp]])

        if self.plot_psd:
            self.ax_i.set_yscale('log')
            self.ax_i.set_xscale('log')
            self.ax_v.set_yscale('log')
            self.ax_v.set_xscale('log')
            self.ax_i.set_xlabel('Frequency [$Hz$]')
            self.ax_v.set_xlabel('Frequency [$Hz$]')
            self.ax_i.set_xlim(self.freqs[1], 500)
            self.ax_v.set_xlim(self.freqs[1], 500)
            max_exponent_v = np.ceil(np.log10(np.max([np.max(l.get_ydata()[1:]) for l in self.ax_v.get_lines()])))
            max_exponent_i = np.ceil(np.log10(np.max([np.max(l.get_ydata()[1:]) for l in self.ax_i.get_lines()])))
            self.ax_i.set_ylim([10**(max_exponent_i - 4), 10**(max_exponent_i + 1)])
            self.ax_v.set_ylim([10**(max_exponent_v - 4), 10**max_exponent_v])
        else:
            self.ax_i.set_yscale('linear')
            self.ax_i.set_xscale('linear')
            self.ax_v.set_yscale('linear')
            self.ax_v.set_xscale('linear')
            self.ax_i.set_xlabel('Time [$ms$]')
            self.ax_v.set_xlabel('Time [$ms$]')
            self.ax_i.set_xlim(self.plot_t_start, self.plot_t_end)
            self.ax_v.set_xlim(self.plot_t_start, self.plot_t_end)
            lim_i = np.max([np.max(np.abs(l.get_ydata())) for l in self.ax_i.get_lines()])
            lim_v_min = np.min([np.min(l.get_ydata()) for l in self.ax_v.get_lines()])
            lim_v_max = np.max([np.max(l.get_ydata()) for l in self.ax_v.get_lines()])
            self.ax_i.set_ylim(-lim_i*1.2, lim_i*1.2)
            self.ax_v.set_ylim(lim_v_min, lim_v_max)

        self.morph_lines[self.prev_intra_comp].set_color('gray')
        self.morph_lines[self.prev_intra_comp].set_lw(1)
        self.morph_lines[self.prev_intra_comp].set_zorder(0)

        self.morph_lines[self.intra_comp].set_color('r')
        self.morph_lines[self.intra_comp].set_lw(3)
        self.morph_lines[self.intra_comp].set_zorder(1)

    def _save_figure(self, *kwargs):
        _folder = self.root_folder_entry.get()
        _figname = self.figname_entry.get()
        if not '.' in _figname:
            _figname += '.png'

        self.fig.savefig(join(_folder, _figname))
        #self.popup.destroy()

    def _calc_lfp_pointsource(self, imem, x=0, y=0, z=0, sigma=0.3):
        r_limit = 0.0001
        r = np.sqrt((self.xmid - x)**2 + (self.ymid - y)**2 + (self.zmid - z)**2)
        r[np.where(r < r_limit)] = r_limit
        Emem = 1e3*1 / (4 * np.pi * sigma) * np.dot(imem.T, 1/r.T).T
        return Emem

    def return_psd(self, sig):
        """ Returns the power and freqency of the input signal"""
        Y = ff.fft(sig)[self.pidxs]
        power = np.abs(Y**2)/len(Y)
        return power

if __name__ == '__main__':
    input_idx = 0
    na_simulation_list = ['NaP_-60_0.0010', 'passive_-60_0.0010']
    ih_simulation_list = 0
    root_dir = join('/home', 'tone', 'work', 'aLFP', 'paper_simulations')

    im_soma_dict = {'cell_name': 'c12861',
                    'sim_type': 'hu',
                    'repeats': 6, # To calculate the number of time steps that is cut removed
                    'root_dir': root_dir,
                    'stimuli': 'white_noise',
                    'input_idx': 0,
                    'simulation_list': ['Im_-60_0.0010', 'passive_-60_0.0010']}

    im_apic_dict = {'cell_name': 'c12861',
                    'sim_type': 'hu',
                    'repeats': 6, # To calculate the number of time steps that is cut removed
                    'root_dir': root_dir,
                    'stimuli': 'white_noise',
                    'input_idx': 963,
                    'simulation_list': ['Im_-60_0.0010', 'passive_-60_0.0010']}

    na_apic_dict = {'cell_name': 'hay',
                    'sim_type': 'hay',
                    'repeats': 6, # To calculate the number of time steps that is cut removed
                    'root_dir': root_dir,
                    'stimuli': 'white_noise',
                    'input_idx': 852,
                    'simulation_list': ['NaP_-60_0.0010', 'passive_-60_0.0010']}

    na_soma_dict = {'cell_name': 'hay',
                    'sim_type': 'hay',
                    'repeats': 6, # To calculate the number of time steps that is cut removed
                    'root_dir': root_dir,
                    'stimuli': 'white_noise',
                    'input_idx': 0,
                    'simulation_list': ['NaP_-60_0.0010', 'passive_-60_0.0010']}

    Ih_apic_dict = {'cell_name': 'hay',
                    'sim_type': 'hay',
                    'repeats': 6, # To calculate the number of time steps that is cut removed
                    'root_dir': root_dir,
                    'stimuli': 'white_noise',
                    'input_idx': 852,
                    'simulation_list': ['Ih_-80_0.0010', 'passive_-80_0.0010']}

    Ih_soma_dict = {'cell_name': 'hay',
                    'sim_type': 'hay',
                    'repeats': 6, # To calculate the number of time steps that is cut removed
                    'root_dir': root_dir,
                    'stimuli': 'white_noise',
                    'input_idx': 0,
                    'simulation_list': ['Ih_frozen_-80_0.0010', 'Ih_-80_0.0010', 'passive_-80_0.0010']}

    hay_dict = {'cell_name': 'hay',
                    'sim_type': 'hay',
                    'repeats': None, # To calculate the number of time steps that is cut removed
                    'root_dir': root_dir,
                    'stimuli': 'white_noise',
                    'input_idx': 455,
                    'simulation_list': ['active_-60_0.0005', 'passive_-60_0.0005']}

    mainen_dict = {'cell_name': 'mainen',
                    'sim_type': 'mainen',
                    'repeats': 1, # To calculate the number of time steps that is cut removed
                    'root_dir': join(''),
                    'stimuli': 'white_noise',
                    'input_idx': 0,
                    'simulation_list': ['0.0010']}

    generic_apic_dict = {'cell_name': 'hay',
                         'sim_type': 'generic',
                         'repeats': 1, # To calculate the number of time steps that is cut removed
                         'root_dir': join('/home', 'tone', 'work', 'aLFP', 'generic_study'),
                         'stimuli': 'wn',
                         'input_idx': 605,
                         'simulation_list': ['-0.5_-80_linear_increase_auto',
                                             '0.0_-80_linear_increase_auto',
                                             '2.0_-80_linear_increase_auto']}

    generic_dict = {'cell_name': 'hay',
                    'sim_type': 'generic',
                    'repeats': 1, # To calculate the number of time steps that is cut removed
                    'root_dir': join('/home', 'tone', 'work', 'aLFP', 'generic_study'),
                    'stimuli': 'wn',
                    'input_idx': 0,
                    'simulation_list': ['-0.5_-80_linear_decrease_auto',
                                        '0.0_-80_linear_decrease_auto',
                                        '2.0_-80_linear_decrease_auto']}

    infinite_axon_dict = {'cell_name': 'infinite_axon',
                          'sim_type': 'infinite_axon',
                          'repeats': 1, # To calculate the number of time steps that is cut removed
                          'root_dir': join(''),
                          'stimuli': 'white_noise',
                          'input_idx': 0,
                          'simulation_list': ['-0.5_-0.5', '0.0_0.0', '2.0_2.0']}

    infinite_neurite_dict = {'cell_name': 'infinite_neurite',
                          'sim_type': 'infinite_neurite',
                          'repeats': 1, # To calculate the number of time steps that is cut removed
                          'root_dir': join('/home', 'tone', 'work', 'aLFP', 'paper_simulations'),
                          'stimuli': 'white_noise',
                          'input_idx': 0,
                          'simulation_list': ['-0.5_-0.5', '0.0_0.0', '2.0_2.0']}

    distributed_dict = {'cell_name': 'hay',
                    'sim_type': 'generic',
                    'repeats': 1, # To calculate the number of time steps that is cut removed
                    'root_dir': join('/home', 'tone', 'work', 'aLFP', 'correlated_population'),
                    'stimuli': 'distributed_synaptic',
                    'input_idx': 'tuft',
                    'simulation_list': ['active_-80mV_c0.00_w0.00010_R400_N1600_cell_00000',
                                        'passive_-80mV_c0.00_w0.00010_R400_N1600_cell_00000']}

    # input_dict = na_soma_dict
    # cell = return_cell(input_idx=input_idx, stimuli="white_noise", weight=0.001)
    # cell = return_cell(input_idx=input_idx, stimuli="white_noise", weight=0.0001, cell_name='infinite_neurite', mu_factor=0.0)
    # cell = return_cell(input_idx=input_idx, stimuli="white_noise", weight=0.0001, cell_name='infinite_axon', mu_factor_1=-0.5, mu_factor_2=-0.5)
    cell = return_cell(input_idx=input_idx, stimuli="white_noise", weight=0.0001, cell_name='mainen', mu_factor_1=0., mu_factor_2=0.)
    # cell = return_cell(input_idx=input_idx, stimuli="white_noise", weight=0.0001, cell_name='infinite_axon', mu_factor_1=0, mu_factor_2=0)
    # cell = return_cell(input_idx=input_idx, stimuli="white_noise", weight=0.0001, cell_name='infinite_axon', mu_factor=2.0)

    # input_dict = generic_apic_dict
    # input_dict = infinite_neurite_dict
    # input_dict = mainen_dict
    # input_dict = distributed_dict
    # sim_exp = SimulationExperiment(**input_dict)

    InteractiveLFP(cell, input_idx)
    # InteractiveLFP(sim_exp, input_dict['input_idx'])
    # InteractiveLFP(sim_exp, 0)
